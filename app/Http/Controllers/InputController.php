<?php

namespace App\Http\Controllers;

use App\Services\InputManagementService;
use Illuminate\Http\Request;

class InputController extends AppBaseController
{
    /**
     * @var InputManagementService
     */
    private InputManagementService $inputManagementService;

    public function __construct(InputManagementService $inputManagementService)
    {
        parent::__construct();
        $this->inputManagementService = $inputManagementService;
    }

    public function inputRequest(Request $request){
        $arrayOfRequest = $request->all();
        try {
            $responseData = $this->inputManagementService->processInputData($arrayOfRequest);
            return $this->response->success($responseData,'success');
        } catch (\Exception $error) {
            return $this->response->error($error->getMessage());
        }
    }
}
