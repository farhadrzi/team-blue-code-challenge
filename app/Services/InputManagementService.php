<?php


namespace App\Services;


use App\Traits\EmojiHandler;
use App\Traits\StringHandler;
use Exception;

class InputManagementService
{
    use EmojiHandler, StringHandler;

    /**
     * @var stringManagementService
     */
    private stringManagementService $stringManagementService;

    public function __construct(stringManagementService $stringManagementService)
    {
        $this->stringManagementService = $stringManagementService;
    }

    /**
     * @param array $inputArray
     * @return float|int|string|array
     * @throws Exception
     */
    public function processInputData(array $inputArray): float|int|string|array
    {
        try {
            if ($this->checkBodyNotEmpty($inputArray)) {
                if (count($inputArray) > 1) {
                    if ($this->checkInputTypeAreSame($inputArray)) {
                        if (count($inputArray) > 2) {
                            $inputArray = $this->selectRandomTwoInput($inputArray);
                        }

                        $inputType = $this->getInputsType($inputArray);
                        return $this->processInputBaseOfType($inputType, $inputArray);
                    }
                } else {
                    return $inputArray[array_keys($inputArray)[0]];
                }
            }
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param array $inputArray
     * @return bool|Exception
     * @throws Exception
     */
    private function checkBodyNotEmpty(array $inputArray): bool|Exception
    {
        if (count($inputArray) > 0) {
            return true;
        } else {
            throw new Exception('there is not input please add some input to process input');
        }
    }

    /**
     * @param array $inputArray
     * @return bool|Exception
     * @throws Exception
     */
    private function checkInputTypeAreSame(array $inputArray): bool|Exception
    {
        $isFirstElement = true;
        $firstElementType = null;
        foreach ($inputArray as $key => $value) {
            if ($isFirstElement) {
                $firstElementType = $this->customInputTypeDetector($value);
                $isFirstElement = false;
            } else {
                if ($firstElementType != $this->customInputTypeDetector($value)) {
                    throw new Exception('all inputs must have same type, please check your input types data');
                }
            }
        }
        return true;
    }

    /**
     * @param $value
     * @return string
     */
    private function customInputTypeDetector($value): string
    {
        $valueType = gettype($value);
        if ($valueType == 'string') {
            if ($this->isEmoji($value)) {
                $valueType = 'emoji';
            }
        }
        return $valueType;
    }

    /**
     * @param array $inputArray
     * @return array
     */
    private function selectRandomTwoInput(array $inputArray): array
    {
        $firstKeyIndex = rand(0, sizeof($inputArray) - 1);
        $secondKeyIndex = rand(0, sizeof($inputArray) - 1);
        while ($secondKeyIndex==$firstKeyIndex){
            $secondKeyIndex = rand(0, sizeof($inputArray) - 1);
        }
        $keyArrays = array_keys($inputArray);
        $firstKey = $keyArrays[$firstKeyIndex];
        $secondKey = $keyArrays[$secondKeyIndex];
        return [$firstKey => $inputArray[$firstKey], $secondKey => $inputArray[$secondKey]];
    }

    /**
     * @param array $inputArray
     * @return string
     */
    private function getInputsType(array $inputArray): string
    {
        foreach ($inputArray as $key => $value) {
            return $this->customInputTypeDetector($value);
        }
    }

    /**
     * @param $type
     * @param $inputArray
     * @return float|int|string
     * @throws Exception
     */
    private function processInputBaseOfType($type, $inputArray)
    {
        switch ($type) {
            case 'string':
                return $this->processStringValue($inputArray);
            case 'integer':
            case 'double':
                return $this->processNumericalValue($inputArray);
            case 'emoji':
                return $this->processEmojiValue($inputArray);
            default:
                throw new Exception('input type not valid for process , please chose another input type');
        }
    }

    /**
     * @param array $inputArray
     * @return string
     */
    private function processEmojiValue(array $inputArray):string{
        $result='';
        foreach ($inputArray as $key=>$value){
            $result = $result.''.$value;
        }
        return $result;
    }

    /**
     * @param array $inputArray
     * @return string
     */
    private function processStringValue(array $inputArray): string
    {
        $keyArrays = array_keys($inputArray);
        return $this->stringManagementService->mergeString($inputArray[$keyArrays[0]], $inputArray[$keyArrays[1]]);
    }

    /**
     * @param array $inputArray
     * @return float|int
     */
    private function processNumericalValue(array $inputArray): int|float
    {
        $totalNumber = 0;
        foreach ($inputArray as $key => $value) {
            $totalNumber += $value;
        }
        return $totalNumber;
    }

    /**
     * The upper method is used to connect two strings together,
     * according to the task that it is said to use dependency injection.
     * But I prefer this method because this method uses the StringHandler trait and makes the code cleaner and more readable.
     */
//    private function processStringValue(array $inputArray):string{
//        $keyArrays = array_keys($inputArray);
//        return $this->mergeString($inputArray[$keyArrays[0]],$inputArray[$keyArrays[1]]);
//    }

}
