<?php


namespace App\Traits;


trait StringHandler
{
    public function mergeString($firstString,$secondString):string{
        return $firstString.''. $secondString;
    }
}
