<?php


namespace App\Traits;


trait EmojiHandler
{
    public function isEmoji($string){
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        preg_match($regexEmoticons, $string, $matches_emo);
        if (!empty($matches_emo[0])) {
            return true;
        }

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        preg_match($regexSymbols, $string, $matches_sym);
        if (!empty($matches_sym[0])) {
            return true;
        }

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        preg_match($regexTransport, $string, $matches_trans);
        if (!empty($matches_trans[0])) {
            return true;
        }

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        preg_match($regexMisc, $string, $matches_misc);
        if (!empty($matches_misc[0])) {
            return true;
        }

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        preg_match($regexDingbats, $string, $matches_bats);
        if (!empty($matches_bats[0])) {
            return true;
        }

        return false;
    }
}
