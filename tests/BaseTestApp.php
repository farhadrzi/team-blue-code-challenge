<?php


namespace Tests;


use ReflectionClass;
use ReflectionException;

class BaseTestApp extends TestCase
{
    use CreatesApplication;
    protected static function getMethod($className,$name) {
        try {
            $class = new ReflectionClass($className);
            $method = $class->getMethod($name);
            $method->setAccessible(true);
            return $method;
        } catch (ReflectionException $e) {
            error_log($e->getMessage());
            return null;
        }

    }
}
