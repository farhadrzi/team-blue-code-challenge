<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InputRequestTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->post('/api/input/request',['input1'=>1,'input2'=>2]);
        $json = json_decode($response->getContent());
        $result = $json->data;
        $this->assertEquals(3, $result);
    }
}
