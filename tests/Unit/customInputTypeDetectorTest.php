<?php

namespace Tests\Unit;

use App\Services\InputManagementService;
use App\Services\stringManagementService;
use Tests\BaseTestApp;

class customInputTypeDetectorTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_for_custom_input_type_detector()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'customInputTypeDetector');
        $result = $class->invokeArgs($classObject, array('farhad'));
        $this->assertEquals('string', $result);
    }
}
