<?php

namespace Tests\Unit;

use App\Traits\StringHandler;
use Tests\BaseTestApp;

class processStringValueTraitTest extends BaseTestApp
{
    use StringHandler;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_merge_string_with_traits()
    {
        $result = $this->mergeString('a', 'b');
        $this->assertEquals('ab',$result);
    }
}
