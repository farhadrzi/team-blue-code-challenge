<?php

namespace Tests\Unit;

use App\Services\InputManagementService;
use App\Services\stringManagementService;
use Tests\BaseTestApp;

class getInputsTypeTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_type_of_each_input()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'getInputsType');
        $result = $class->invokeArgs($classObject, array(['input1'=>1,'input2'=>2]));
        $this->assertEquals('integer', $result);
    }
}
