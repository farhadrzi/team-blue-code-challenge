<?php

namespace Tests\Unit;


use App\Services\InputManagementService;
use App\Services\stringManagementService;
use Tests\BaseTestApp;

class processInputBaseOfTypeTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_process_input_base_of_type_of_each_input()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'processInputBaseOfType');
        $result = $class->invokeArgs($classObject, array('integer',['input1'=>1,'input2'=>2]));
        $this->assertEquals(3, $result);
    }
}
