<?php

namespace Tests\Unit;

use App\Services\InputManagementService;
use App\Services\stringManagementService;
use PHPUnit\Framework\TestCase;
use Tests\BaseTestApp;

class checkInputBodyNotEmptyTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_input_body_not_empty()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'checkBodyNotEmpty');
        $result = $class->invokeArgs($classObject, array(['input1'=>1]));
        $this->assertTrue($result);
    }
}
