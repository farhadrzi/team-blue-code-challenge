<?php

namespace Tests\Unit;

use App\Services\InputManagementService;
use App\Services\stringManagementService;
use Tests\BaseTestApp;

class selectRandomTwoInputTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_select_two_random_of_multi_input()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'selectRandomTwoInput');
        $result = $class->invokeArgs($classObject, array(['input1'=>'a','input2'=>'b','input3'=>'c']));
        $this->assertCount(2, $result);
    }
}
