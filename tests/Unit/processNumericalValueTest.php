<?php

namespace Tests\Unit;


use App\Services\InputManagementService;
use App\Services\stringManagementService;
use Tests\BaseTestApp;

class processNumericalValueTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_process_numerical_value_for_numerical_values()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'processNumericalValue');
        $result = $class->invokeArgs($classObject, array(['input1'=>1,'input2'=>2]));
        $this->assertEquals(3, $result);
    }
}
