<?php

namespace Tests\Unit;

use App\Services\InputManagementService;
use App\Services\stringManagementService;
use Tests\BaseTestApp;

class checkInputTypeAreSameTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_check_input_type_are_same()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'checkInputTypeAreSame');
        $result = $class->invokeArgs($classObject, array(['input1'=>1,'input2'=>1]));
        $this->assertTrue($result);
    }
}
