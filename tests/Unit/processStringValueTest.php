<?php

namespace Tests\Unit;


use App\Services\InputManagementService;
use App\Services\stringManagementService;
use Tests\BaseTestApp;

class processStringValueTest extends BaseTestApp
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_process_string_value()
    {
        $classObject = new InputManagementService(new stringManagementService());
        $class = self::getMethod(get_class($classObject), 'processStringValue');
        $result = $class->invokeArgs($classObject, array(['input1'=>'a','input2'=>'b']));
        $this->assertEquals('ab', $result);
    }
}
